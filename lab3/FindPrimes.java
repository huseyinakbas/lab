public class FindPrimes {
	public static void main(String[] args) {
		int max = Integer.parseInt(args[0]);
		
		// Print the numbers less than max
		// For each number, less than max
		for(int number = 2; number < max; number++) {

			// let divisor = 2;
			int divisor = 2;

			// let isPrime = true;
			boolean isPrime = true;
			// while (divisor < number)
			while (divisor < number && isPrime) {
				
				// if the number is divisible by divisor
				if (number % divisor == 0)
					// isPrime = false;
					isPrime = false;
					// increment divisor
				divisor++;
			
		}
		// If the number is prime
		if(isPrime){
			// print the number
			System.out.println(number + " is prime!");
		}
		
	}
}
}
