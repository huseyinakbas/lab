import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input
		int count = 1; //counts the number of guesses

		while (guess != number && guess != -1) {
			System.out.println("Sorry!");
			
			count += 1;
			if (guess < number) {
				System.out.println("Your guess is too low.");
			} else {
				System.out.println("Your guess is too high.");
			}
			System.out.println("Type -1 to quit or guess again: ");
			guess = reader.nextInt();

		}

		if (guess == number){
			System.out.println("Congratulations!");
			System.out.println("You guessed the number in " + count + " guesses.");
		}
		else if (guess == -1){
			System.out.println("The number was " + number);
			System.out.println("Bye!");
		}


		reader.close(); //Close the resource before exiting
	}
	
	
}