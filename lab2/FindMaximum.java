public class FindMaximum {

	public static void main(String[] args){
		int value1 = Integer.parseInt( args[0]);
        int value2 = Integer.parseInt(args[1]);
        int result;


        result = value1 > value2 ? value1 : value2;  // if this is true the first one will return, otherwise second will be return

        System.out.println(result);

	}
}