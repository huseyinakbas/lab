public class FindMinimum {

    public static void main(String[] args){
        int value1 = Integer.parseInt( args[0]);
        int value2 = Integer.parseInt(args[1]);
        int value3 = Integer.parseInt(args[2]);
        int result;


        result = value1 < value2 ? value1 : value2;  // if this is true the first one will return, otherwise second will be return
        result = result < value3 ? result : value3;
        System.out.println(result);

    }
}