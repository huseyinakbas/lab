import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		int moveCount = 0;
		int currentPlayer = 0;
		printBoard(board);

		while (moveCount < 9) {
			System.out.print("Player " + (currentPlayer + 1) + " enter row number: ");
			int row = reader.nextInt();
			System.out.println("Player " + (currentPlayer + 1) + " enter column number: ");
			int col = reader.nextInt();

			if(row > 0 && row < 4 && col > 0 && col < 4 && board[row - 1][col - 1] == ' ') {			
				board[row - 1][col - 1] = currentPlayer == 0 ? 'X' : 'O';

				currentPlayer = (currentPlayer + 1) % 2;
				moveCount++;

				printBoard(board);

				boolean win = checkBoard(board, row -1, col - 1);
				if(win) {
					System.out.println("Player " + (currentPlayer) + " wins!");
					break;
				} else if (moveCount == 9) {
					System.out.println("Draw!");
				}


			} else {
				System.out.println("Invalid move!");
			}
			// if(currentPlayer == 0){
			// 	board[row - 1][col - 1] = 'X';
			// }
			// else{
			// 	board[row - 1][col - 1] = 'O';
			// }

		}

		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	public static boolean checkBoard(char[][] board, int row, int col) {
		char symbol = board[row][col];

		boolean win = true;
		for (int c = 0; c < 3; c++) {
			if ((board[row][c]) != symbol) {
				win = false;
				break;
			}
		}
		if (win)
			return true;

		win = true;
		for (int r = 0; r < 3; r++) {
			if ((board[r][col]) != symbol) {
				win = false;
				break;
			}
		}
		if (win)
			return true;

		// diagonal #1
		if (row == col) {
			win = true;
			for (int i = 0; i < 3; i++) {
				if ((board[i][i]) != symbol) {
					win = false;
					break;
				}
			}
		}
		if (win)
			return true;
		
		// diagonal #2
		if (row + col == 2) {
			win = true;
			for (int i = 0; i < 3; i++) {
				if ((board[i][2 - i]) != symbol) {
					win = false;
					break;
				}
			}
		}
		if (win)
			return true;



		return false;
	}

}